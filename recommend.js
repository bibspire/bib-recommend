const fs = require('fs');
const cbor = require('cbor');
const level = require('level');
const assert = require('assert');
const _ = require('lodash');
const http = require('http');
const qs = require('qs');

async function title(pid) {
  const meta = await metadata(pid)
  return meta.titleFull[0] + ' - ' + creators[ids[pid]]
}

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
console.time('init');
const pids = cbor.decode(fs.readFileSync('pids.cbor'));
const ids = {};
for(const i in pids) {
  ids[pids[i]] = +i;
}
const counts = cbor.decode(fs.readFileSync('counts.cbor'));
const creators = cbor.decode(fs.readFileSync('creators.cbor'));
const uid_id = level('db_uid_id.leveldb', { valueEncoding: 'binary', keyEncoding: 'binary', createIfMissing: false})
const id_uid = level('db_id_uid.leveldb', { valueEncoding: 'binary', keyEncoding: 'binary', createIfMissing: false})
console.timeEnd('init');

function nRandom(arr, n) {
  if(arr.length <= n) {
    return arr;
  }
  const newArr = [];
  for(let i = 0; i < n ; ++i) {
    const pos = Math.random() * arr.length | 0;
    newArr.push(arr[pos]);
    arr[pos] = arr[arr.length -1];
    arr.pop();
  }
  return newArr;
}

async function recommend({pid, max_patrons = 300, one_per_creator = true, limit = 20, longtailedness}) {

  const t0 = Date.now();
  one_per_creator = !((!one_per_creator) || one_per_creator === 'false');
  if(longtailedness === undefined) {
    longtailedness = 0.7
  }
  longtailedness = +longtailedness

  max_patrons = max_patrons || 500;
  limit = limit || 20;

  const id = ids[pid];
  if(typeof id !== 'number') {
    return []
  }

  let users = cbor.decode(await id_uid.get(cbor.encode(id)))
  users = nRandom(users, max_patrons);
  let userLoans = await Promise.all(users.map(user => uid_id.get(cbor.encode(user))))
  userLoans = userLoans.map(o => Array.from(new Set(cbor.decode(o)[0])))
  userLoans = _.toPairs(_.countBy(_.flatten(userLoans))).map(([id, count]) => [+id, count])

  const weightFn = ([coloan_id, count]) => count / Math.pow(counts[coloan_id] + 100, longtailedness);


  userLoans = _.reverse(_.sortBy(userLoans , weightFn ))
  if(one_per_creator) {
    const creators_seen = new Set();
    userLoans = userLoans.filter(([id, _]) => {
      if(creators[id] && creators_seen.has(creators[id])) {
        return false;
      }
      creators_seen.add(creators[id])
      return true;
    });
  }
  userLoans = userLoans.slice(0, limit);
  userLoans = userLoans.map(([id, _]) => pids[id])
  return {related: userLoans.slice(1), popularity: counts[id], time_taken: Date.now() - t0};
}

function randomPid() {
  return pids[Math.random() * Math.random() * pids.length | 0];
}

http.createServer(async (req, res) => {
  if(req.url.startsWith('/v1/')) {
    res.writeHead(200, {'Content-Type': 'application/json'});
    if(req.url.startsWith('/v1/recommend?')) {
      const args = qs.parse(req.url.replace('/v1/recommend?', ''))
      return res.end(JSON.stringify(await recommend(args)))
    }
    if(req.url === '/v1/randompid') {
      return res.end(JSON.stringify(randomPid()))
    }
  }
  if(req.url === '/') {
    res.writeHead(200, {'Content-Type': 'text/html'});
    return fs.readFile('demo.html', (err, result) => {
      res.end(result);
    });
  }
  if(req.url === '/demo.js') {
    res.writeHead(200, {'Content-Type': 'application/javascript'});
    return fs.readFile('demo.js', (err, result) => {
      res.end(result);
    });
  }
  if(req.url === '/icon.png') {
    res.writeHead(200, {'Content-Type': 'image/png'});
    return fs.readFile('icon.png', (err, result) => {
      res.end(result);
    });
  }
  res.writeHead(404, {'Content-Type': 'text/plain'});
  res.end('not found');
}).listen(8080);
