FROM node:latest

COPY . /app
WORKDIR /app
RUN rm -rf /app/.git /app/node_modules
RUN npm install
ENTRYPOINT ["node", "recommend.js"]
EXPOSE 8080
