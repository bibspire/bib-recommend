let scheduled = false;
let running = false;
async function demo() {
  if(running) {
    scheduled = true;
    return;
  }
  running = true;
  do {
    scheduled = false;
    for(const elem of document.querySelectorAll('.example')) {
      const pid = elem.childNodes[0].value

      let {related, popularity, time_taken} = await (await fetch(`https://demo.bibspire.dk/v1/recommend?pid=${pid}&limit=${limit.value}&one_per_creator=${one_per_creator.checked}&longtailedness=${longtailed.value}&max_patrons=${max_patrons.value}`)).json()
      related = related.map(o => o.pid)

      while(elem.childNodes.length > 1) {
        elem.childNodes[1].remove()
      }
      let childElem = document.createElement('pre');
      childElem.innerHTML = `recommender time: ${time_taken}ms   ${pid} popularity: ${popularity}   longtailedness: ${longtailed.value}`
      elem.appendChild(childElem);

      childElem = cover(pid, 'current');
      elem.appendChild(childElem);

      function cover(pid, imgclass) {
        const a = document.createElement('a')
        a.target = '_blank'
        a.href = 'https://demo.bibspire.dk/bib?pid=' + pid
        const img = document.createElement('img');
        img.src = `https://demo.bibspire.dk/bib/cover?cover=${pid}`
        img.className = imgclass || 'cover'
        a.appendChild(img);
        return a
      }
      for(const recommendPid of related) {
        elem.appendChild(cover(recommendPid))
      }
    }
  } while(scheduled);
  running = false;
}

async function randomize() {
  for(const elem of document.querySelectorAll('.example input')) {
    elem.value = await (await fetch('/v1/randompid')).json()
  }

  demo()
}

async function init() {
  for(const elem of document.querySelectorAll('input')) {
    elem.onchange = demo;
  }

  randomize();
}
document.getElementById('randomize').onclick = randomize;
init();
